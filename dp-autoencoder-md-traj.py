from keras.layers import Input, Dense
from keras.models import Model
from keras import optimizers
from sklearn.preprocessing import scale
import numpy as np
import scipy.io as sio
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

mat_train = sio.loadmat('1h9t_md_110_trunc.mat')
x_train = mat_train['arr']
x_train = x_train.reshape((x_train.shape[0]*x_train.shape[1], x_train.shape[2]))
x_train = np.transpose(x_train)
x_train = scale(x_train)
print ('data size = ', x_train.shape)

input_dim = x_train.shape[1] # = 26979

# this is our input placeholder
input_ins = Input(shape=(input_dim,))
encoded = Dense(1024, activation='sigmoid')(input_ins)
encoded = Dense(2, activation='sigmoid')(encoded)
decoded = Dense(1024, activation='sigmoid')(encoded)
decoded = Dense(input_dim, activation='sigmoid')(decoded)


# this model maps an input to its reconstruction
autoencoder = Model(input_ins, decoded)

# this model maps an input to its encoded representation
encoder = Model(input_ins, encoded)
# create a placeholder for an encoded (32-dimensional) input
encoded_input = Input(shape=(2,))
# retrieve the last layer of the autoencoder model
decoder_layer = autoencoder.layers[-1](autoencoder.layers[-2](encoded_input))
# create the decoder model
decoder = Model(encoded_input, decoder_layer)

autoencoder.compile(optimizer='adadelta', loss='mean_squared_error')

autoencoder.fit(x_train, x_train,
                epochs=100,
                batch_size=256,
                shuffle=True)

encoded_ins = encoder.predict(x_train)
decoded_ins = decoder.predict(encoded_ins)

print('reduced data = ', encoded_ins[1:2000:100])

# Visualization
x = encoded_ins[1:2000:100,0]
y = encoded_ins[1:2000:100,1]
plt.plot(x, y, '-x')
plt.annotate('1', (x[0], y[0]))
plt.annotate('1001', (x[10], y[10]))
plt.annotate('1991', (x[19], y[19]))
#plt.show()
plt.savefig('dp-autoencoder-analysis-tanh.png')
