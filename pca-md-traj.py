import numpy as np
from sklearn.decomposition import PCA
import scipy.io as sio
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

mat_train = sio.loadmat('1h9t_md_110_trunc.mat')
x_train = mat_train['arr']
x_train = x_train.astype('float32')
x_train = x_train.reshape((x_train.shape[0]*x_train.shape[1], x_train.shape[2]))
x_train = np.transpose(x_train)
print ('data size = ', x_train.shape)

pca = PCA(n_components=2)
encoded_ins = pca.fit_transform(x_train)
print('reduced data = ', encoded_ins[1:2000:100])

# Visualization
x = encoded_ins[1:2000:100,0]
y = encoded_ins[1:2000:100,1]
plt.plot(x, y, '-x')
plt.annotate('1', (x[0], y[0]))
plt.annotate('1001', (x[10], y[10]))
plt.annotate('1991', (x[19], y[19]))
#plt.show()
plt.savefig('pca-analysis.png')
